<?php

class ReportModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getReports($employeeId) {
        $this->db->select("employeeNumber,concat(firstName,' ',lastName) as name,jobTitle,employees.officeCode,offices.city");
        $this->db->from("employees");
        $this->db->join('offices', 'employees.officeCode = offices.officeCode');
        $this->db->where('employees.employeeNumber', $employeeId);
        $query = $this->db->get();
        return $query->result('array');
    }

    public function getProductsLine($employeeId) {
        $this->db->select("
                productlines.productLine,
                textDescription
                ");
        $this->db->from("customers");
        $this->db->join('orders', 'customers.customerNumber = orders.customerNumber');
        $this->db->join('orderdetails', 'orders.orderNumber = orderdetails.orderNumber');
        $this->db->join('products', 'orderdetails.productCode = products.productCode');
        $this->db->join('productlines', 'products.productLine = productlines.productLine');
        $this->db->where('customers.salesRepEmployeeNumber', $employeeId);
        $this->db->group_by('productlines.productLine');
        $query = $this->db->get();
        return $query->result('array');
    }
 
    public function getProducts($productline){
        $this->db->select("
            orderdetails.productCode,
            productName,
            quantityInStock as quantity,
            ROUND(SUM(priceEach),2) as sales,
            (SELECT COUNT(*) FROM orderdetails WHERE productCode = orderdetails.productCode) as numberOfCustomerBought");
        $this->db->from("products");
        $this->db->join('orderdetails', 'products.productCode = orderdetails.productCode');
        $this->db->where('products.productLine', $productline);
        $this->db->group_by('orderdetails.productCode');
        $query = $this->db->get();
        return $query->result('array');
    }

    public function getOverAllProductSales(){
        $this->db->select("ROUND(SUM(priceEach),2) as overall_sales");
        $this->db->from("orderdetails");
        $query = $this->db->get();
        return $query->result('array');
    }
  
}