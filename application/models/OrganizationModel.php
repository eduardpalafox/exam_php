<?php

class OrganizationModel extends CI_Model {

    protected $table = 'employees';

    public function __construct() {
        parent::__construct();
    }

    public function get_all() {
        $this->db->select("employeeNumber,concat(firstName,' ',lastName) as name,jobTitle");
        $this->db->from($this->table);
        $this->db->where('reportsTo', null);
        $query = $this->db->get();
        return $query->result('array');



    }

    public function get_employee_under($supervisorId){
        $this->db->select("employeeNumber,concat(firstName,' ',lastName) as name,jobTitle");
        $this->db->from($this->table);
        $this->db->where('reportsTo', $supervisorId);
        $query = $this->db->get();
        return $query->result('array');
    }
}