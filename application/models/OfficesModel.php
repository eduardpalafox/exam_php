<?php

class OfficesModel extends CI_Model {

    protected $table = 'offices';

    public function __construct() {
        parent::__construct();
    }

    public function get_all_offices() {
        $this->db->select("officeCode,city");
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result('array');
    }

    public function get_employees($employeeId){
        $this->db->select("employeeNumber,concat(firstName,' ',lastName) as name,jobTitle");
        $this->db->from("employees");
        $this->db->where('officeCode', $employeeId);
        $query = $this->db->get();
        return $query->result('array');
    }
}