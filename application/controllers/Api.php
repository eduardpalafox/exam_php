<?php 
   class Api extends CI_Controller {
  
   		public function __construct() {
          parent::__construct();
          $this->load->model('ReportModel');
          $this->load->model('organizationModel');
          $this->load->model('officesModel');

      }
      public function organizations() { 
        $data = $this->organizationModel->get_all();
        if($data){
          foreach ($data as $key => $value) {
            $data[$key]["employeeUnder"] =  $this->get_under($value['employeeNumber']);
          }
        }
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
      } 

       public function get_under($employeeNumber) { 
          $tempEmployee = $this->organizationModel->get_employee_under($employeeNumber);
          if($tempEmployee){
            foreach ($tempEmployee as $key => $value) {
              $tempEmployee[$key]['employeeUnder'] = $this->get_under($value['employeeNumber']);
            }
          }
          return $tempEmployee;
      } 


      /*OFFICES*/
      public function offices() { 
        $data = $this->officesModel->get_all_offices();
        if($data){
          foreach ($data as $key => $value) {
            $data[$key]["employees"] =  $this->officesModel->get_employees($value['officeCode']);
          }
        }
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));

      }

      /*sales Report*/
      public function sale_report($employeeNumber = null) { 

        if(is_null($employeeNumber)){
          $data = $this->ReportModel->getOverAllProductSales()[0];
          return $this->output
              ->set_content_type('application/json')
              ->set_status_header(200)
              ->set_output(json_encode($data));
        }

        $data = $this->ReportModel->getReports($employeeNumber);
        
        if($data){
          foreach ($data as $key => $value) {
            $tempProductlines = $this->getProduclines( $value['employeeNumber'] );

            $sales = 0;
            if($tempProductlines){
              foreach($tempProductlines as $pkey => $pvalue){
                $sales += $pvalue['sales'];
              }
            }
            $sales = ROUND($sales,2);
            $data[$key]["sales"] = $sales;
            $data[$key]["productLines"] = $tempProductlines;


          }
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
      } 


      public function getProduclines($employeeNumber){
        $tempProductlines = $this->ReportModel->getProductsLine( $employeeNumber );

        if($tempProductlines){
          foreach ($tempProductlines as $key => $value) {
            $tempProducts = $this->ReportModel->getProducts( $value['productLine'] );

            $quantity = 0;
            $sales = 0;

            if($tempProducts){
              foreach($tempProducts as $pkey => $pvalue){
                $quantity += $pvalue['quantity'];
                $sales += $pvalue['sales'];
              }
            }
            $sales = ROUND($sales,2);
            $tempProductlines[$key]["quantity"] = $quantity;
            $tempProductlines[$key]["sales"] = $sales;
            $tempProductlines[$key]["products"] = $tempProducts;

          }
        }

        return $tempProductlines;

      }


   } 
?>